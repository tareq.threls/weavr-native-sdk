//
//  OkayWrapper.swift
//  WeavrComponents
//
//  Created by Tareq Islam on 4/7/22.
//

import Foundation
import PSA




public class OkayWrapper{
    
    private let okayConfig = OkayConfig()
    
    public func enrollDevice(invisibly : Bool,  resourceProvider : ResourceProvider?,  completion : @escaping (PSASharedStatuses) -> Void){

        PSA.startEnrollment(withHost: okayConfig.okayServerUrl , invisibly: invisibly, installationId: okayConfig.installationId, resourceProvider:  resourceProvider == nil ? ConfigurableResourceProvider() : resourceProvider!  ,  pubPssBase64: okayConfig.pubPssBase64, idCompletion: completion)

    }
 
}

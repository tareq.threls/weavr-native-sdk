//
//  CardDialogVC.swift
//  components-example
//
//  Created by Tareq on 3/5/22.
//

import UIKit
import WeavrComponents

class CardDialogVC: UIViewController {

 
    
    @IBOutlet weak var cardNumberLabel: SecureCardNumberLabel!
    
    @IBOutlet weak var cvvLabel: SecureCVVLabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.cardNumberLabel.enableCopyTextToClipboardOnClick(enabled: true, completion: { msg in
            print(msg)
        })
        self.cardNumberLabel.enableCopyTextToClipboardOnLongClick(enabled: true, completion: { msg in
            print(msg)
        })
        
      
        self.cvvLabel.enableCopyTextToClipboardOnClick(enabled: false, completion: nil)
        self.cardNumberLabel.setTokeniseText( toDetokenise: "i1OmZxzvg8IBgIqGyw8ACw==", callback: {res in

            switch res {

            case .success(let data):
                print(data)
            case .failure(let error):
                print(error)
            }

        } )
        
        self.cvvLabel.setTokeniseText( toDetokenise: "TNbT9c7K304BgIqH+0AACw==", callback: {res in
            
            switch res {

            case .success(let data):
                print(data)
            case .failure(let error):
                print(error)
            }

        })
    }


   

}

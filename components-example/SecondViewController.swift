//
//  SecondViewController.swift
//  components-example
//
//  Created by Tareq on 23/4/22.
//

import WeavrComponents
import UIKit
class SecondViewController: UIViewController {
    
   
    @IBOutlet var PinCodeField: SecureCardPinField!
    @IBOutlet var PassCodeField: SecurePasscodeField!
    @IBOutlet var passwordField: SecurePasswordField!
    
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var PassCodeLabel: UILabel!
    
    @IBOutlet weak var CardPinLabel: UILabel!
    
    @IBOutlet var secureLabel: SecureCardPinLabel!
    
    @IBOutlet var submit: UIButton!
    @IBAction func submitBtn(_ sender: Any) {
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "CardDialogVC") as! CardDialogVC
        
        
        vc.modalPresentationStyle = .formSheet //for full screen mode like navigation else it would look like modal sheet
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion:  nil)
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secureLabel.text = "Secure Text"
        passwordField.addDoneButtonOnKeyboard()
        PassCodeField.addDoneButtonOnKeyboard()
        PinCodeField.addDoneButtonOnKeyboard()
    }
    
    
    @IBAction func onPasswordSubmit(_ sender: Any) {
        passwordField.createToken( callBack: { res in
            switch res {
            case let .success(data):
                
                self.passwordLabel.text = data.first?.value
            case let .failure(res):
                print(res.message ?? "")
            }
        })
        
    }
    @IBAction func onPasscodeSubmit(_ sender: Any) {
        PassCodeField.createToken( callBack: { res in
            switch res {
            case let .success(data):
             //   self.PassCodeLabel.text = data.first?.value
                self.PassCodeLabel.text = data["PassKey"] as? String
                
            case let .failure(res):
                print(res.message ?? "")
            }
        })
    }
    @IBAction func onCardPinSubmit(_ sender: Any) {
        
        PinCodeField.createToken( callBack: { res in
            switch res {
            case let .success(data):
                
                self.CardPinLabel.text = data.first?.value
                
                self.secureLabel.setTokeniseText(toDetokenise: data.first?.value ?? "" , callback: { res in
                    switch res {
                    case .success(let data):
                        print(data)
                    case .failure(let error):
                        print(error.message ?? "")
                    }
                })
            case let .failure(res):
                print(res.message ?? "")
            }
        })
    }
}

//
//  ViewController.swift
//  components-example
//
//  Created by Tareq on 12/4/22.
//

import WeavrComponents
import UIKit
import Toast_Swift
import Alamofire


class ViewController: UIViewController {
    private var tokenizePasswod: String?
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: SecurePasscodeField!
    @IBOutlet var confirmPasswordField: SecurePasswordField!
    
    lazy var securePasswordField: SecureTextField = {
        let textFiled = SecureTextField()
        textFiled.placeholder = "Password"
        textFiled.frame = CGRect(x: 10, y: 100, width: 300, height: 30)
        textFiled.font = UIFont.boldSystemFont(ofSize: 12)
        textFiled.textAlignment = .left
        textFiled.textColor = .blue
        textFiled.layer.borderColor = UIColor.blue.cgColor
        textFiled.layer.borderWidth = 1
        return textFiled
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //cfE2+PcFh20BbxPI9+IACQ=="
        UXComponents.initialize(env: ENV.QA, uiKey: "cfE2+PcFh20BbxPI9+IACQ==")
        
        //view.addSubview(securePasswordField)
        
        passwordField.isSecureTextEntry = false // test case : 1 ,result it should not work
        
        // for test
        passwordField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        passwordField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        passwordField.addTarget(self, action: #selector(textFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
        passwordField.addDoneButtonOnKeyboard()
        confirmPasswordField.addDoneButtonOnKeyboard()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print("text changed \(String(describing: textField.text))")
    }
    
    @objc func textFieldDidEnd(_ textField: UITextField) {
        print("text changed End\(String(describing: textField.text))")
    }
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        print("text changes Begin \(String(describing: textField.text))")
    }
    
    @objc func textFieldDidEndOnExit(_ textField: UITextField) {
        print("text changed EndOnExit \(String(describing: textField.text))")
    }
    
    @IBAction func tokenizeBtn(_ sender: Any) {
        
        print(passwordField.text)
        print(passwordField.attributedText)
        // for creating the group
        let group1 = UXComponents.createGroup(components: [passwordField, confirmPasswordField])
        group1.removeComponents(components: [confirmPasswordField])
        group1.addComponents(components: [confirmPasswordField, passwordField])
        
        print(group1.match())
        
        // for checking group's data match
        if group1.match() {
            view.makeToast("Passwords matched")
            // for making single call for all the components in this group
            group1.createToken(fieldKeys: ["PassKey0", "PassKey1", "PassKey2"], callBack: {
                weavrResponse in
                switch weavrResponse {
                case let .success(data): // the dictionary provides [String : String?], where keys are the passKeys and values are the tokenized values
                    
                    let encodedData = try? JSONEncoder().encode(data)
                    let jsonString = String(data: encodedData!, encoding: .utf8)
                    print(jsonString!)
                    
                    // as dictionary is randomly ordered due to hasing, we are sorting the dictionary by keys
                    let sortedData = data.sorted(by: { $0.key < $1.key })
                    
                    for (key, value) in sortedData {
                        print("Key: \(key) \(value ?? "")")
                    }
                    
                    self.tokenizePasswod = sortedData.first?.value ?? ""
                    self.view.makeToast(self.tokenizePasswod)
                    
                case let .failure(err):
                    print("\(err.code ) \(err.message) \(err.body ?? "")")
                }
            })
        } else {
            view.makeToast("Password not match")
        }
        
        /*          passwordField.createToken(){  weavrResponse   in
         switch weavrResponse {
         
         case .success(let  data  ): //the dictionary provides [String : String?], where keys are the passKeys and values are the tokenized values
         
         let encodedData =  try? JSONEncoder().encode(data)
         let jsonString = String(data: encodedData!, encoding: .utf8)
         print(jsonString)
         
         let passKeys = data.map{ $0.value}
         
         for   el in passKeys {
         print(el ?? "")
         }
         
         self.tokenizePasswod = passKeys[0] ?? ""
         
         case .failure(let error):
         print(error)
         }
         }
         */
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        
        loginUser(email: emailField.text ?? "", pass: tokenizePasswod ?? "")
//        UXComponents.weavrLogin(email: emailField.text ?? "", tokenizePassword: tokenizePasswod ?? "", completion: { res in
//
//            switch res {
//            case let .success(token):
//                print(token)
//
//                UXComponents.setUserToken(token: token, completion: { res in
//                    switch res {
//                    case let .success(data):
//                        print(data)
//                        self.view.makeToast("Successfully, User token has been set")
//                    case let .failure(err):
//                        print(err.code ?? "" )
//                    }
//
//                })
//            case let .failure(error):
//                    print(error.body)
//            }
//
//        })
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "secondVC") as! SecondViewController
      
        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    private func loginUser(email:String, pass : String){
        let headers = ["Accept": "application/json",
                "Content-Type": "application/json"
                ]
        
        let params = [
            
                "email": email,
                "password": [
                    "value": pass
                ]
            
        ] as [String : Any]
                    
    let url = "https://qa.onvirtual.cards/proxy/multi/login_with_password"
     AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
      .validate()
      .responseJSON{(response) in
        print (response)
        switch response.result{
        case .success:
            if let json = response.value as? [String:Any] {
                    print(json["token"])
                UXComponents.setUserToken(token: json["token"] as! String, completion: { res in
                    switch res {
                    case let .success(data):
                        print(data)
                        self.view.makeToast("Successfully, User token has been set")
                    case let .failure(err):
                        print(err.message ?? "" )
                    }
                    
                })
                 }
            
           
        // value is a simple iOS dictionary of type [String: Any] from which you can extract additional information
        case .failure(let error):
         print (error)
       }
     }
    }

}

//
//  components_exampleUITests.swift
//  components-exampleUITests
//
//  Created by Tareq on 2/5/22.
//

import XCTest

class components_exampleUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        
      
        let emailTextField = app.textFields["Email"]
        XCTAssertTrue(emailTextField.exists)
        emailTextField.tap()
        emailTextField.typeText("Gary@test.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        XCTAssertTrue(passwordSecureTextField.exists)
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Pass1234!")
        
                
      
        
        let confirmPasswordSecureTextField = app.secureTextFields["Confirm Password"]
        XCTAssertTrue(confirmPasswordSecureTextField.exists)
        confirmPasswordSecureTextField.tap()
        confirmPasswordSecureTextField.typeText("Pass1234!")
       
        
        let tokeniseButton = app.buttons["Tokenize"]
        XCTAssertTrue(tokeniseButton.exists)
        tokeniseButton.tap()
//        expectation(for: NSPredicate(format: "exists == 1" ), evaluatedWith: tokeniseButton, handler: nil)
//
//        waitForExpectations(timeout: 5)
                        //                let promise = expectation(description: "Status code: 200")
//        wait(for: [promise], timeout: 5)
        
        
//       let toast =  app.staticTexts["Key: "]
//       // XCTAssertTrue(toast.exists)
//        expectation(for: NSPredicate(format: "exists == 1" ), evaluatedWith: toast, handler: nil)
//        waitForExpectations(timeout: 5)
        wait(for: [], timeout: 5)
       
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}

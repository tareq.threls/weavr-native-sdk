//
//  componentsTests.swift
//  componentsTests
//
//  Created by Tareq on 2/5/22.
//

import XCTest
import Alamofire
@testable import components

class componentsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
       // UXComponents.initialize(env: ENV.QA, uiKey: "cfE2+PcFh20BbxPI9+IACQ==")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAnonTokenize() throws {
        let promise = expectation(description: "Status code: 200")
        UXComponents.initialize(env: ENV.QA, uiKey: "cfE2+PcFh20BbxPI9+IACQ==")
        
        XCTAssertNil(SessionUtil.shared.getUIKey() )
        


        /// success case
        WeavrRepo().tokenizeString(targetText: ["Pass1234!"],programmeKey: "cfE2+PcFh20BbxPI9+IACQ==",  completion: { res in
            
            switch res {
                
            case .success(let data):
                XCTAssertEqual(data.count, 1)
                promise.fulfill()
            case .failure(let err):
              XCTAssertNil(err)
                XCTFail("Status code: \(err)")
            }
        })
        
        wait(for: [promise], timeout: 5)
        
//        let promise2 = expectation(description: "Status code: 500")
//        ///Failed case porgramme key error
//        WeavrRepo().tokenizeString(targetText: ["Pass1234!"],programmeKey: "cfE2+PcFh20BbxPI9+IAC",  completion: { res in
//
//            switch res {
//
//            case .success(let data):
//                XCTAssertNotEqual(data.count, 1)
//                XCTFail("Status code: \(data)")
//            case .failure(let err):
//              XCTAssertNotNil(err)
//                promise2.fulfill()
//            print(err)
//            }
//        })
//
//
//        wait(for: [ promise2], timeout: 5)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
